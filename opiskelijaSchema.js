//Luodaan schema opiskelijasta, jota tietokanta käyttää.
//Tästä opiskelijasta luodaan sitten model, joka palautetaan muun ohjelman käyttöön.

var mongoose = require('mongoose');

var opiskelijaSchema = new mongoose.Schema({
    'opiskelijanumero': { type: String, required: true, unique: true },
    'nimi': { type: String, required: true },
    'email': String,
    'opintopisteet': Number,
    'kurssit': [{
        '_id': false, //Ehkäisee turhan _id:n luomisen
        'kurssinimi': String,
        'laajuus': Number,
        'arvosana': Number
    }]
});

var op = mongoose.model('opiskelija', opiskelijaSchema);

module.exports = op;