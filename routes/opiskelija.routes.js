//routes tarjoaa http reitit, joita pitkin pyyntö ja sen parametrit ohjataan oikealle funktiolle.

module.exports = (app) => {
    const opiskelijat = require('../controllers/opiskelija.controller.js');

    app.get('/opiskelijat', opiskelijat.findAll);

    app.get('/opiskelijat/:opId', opiskelijat.findOne);

    app.get('/opiskelijatUnder100Sp', opiskelijat.findAllWithUnder100Sp);

    app.post('/opiskelijat', opiskelijat.create);

    app.post('/opiskelijat/:opId', opiskelijat.updateOne);

    app.post('/opiskelijatAddCourse/:opId', opiskelijat.addCourse);

    app.post('/opiskelijat/:opId/:kursNimi', opiskelijat.editCourse);

    app.delete('/opiskelijat/:opId', opiskelijat.deleteOne);

}