var jwt = require('jsonwebtoken');

const User = require('../user.js');

//Secretin pitäisi olla pidempi, vaikeampi ja jossain ulkoisessa tiedostossa.
const secret = 'salaisuus';

//Luodaan JWT token, mikäli autentikointikutsussa oli oikea käyttäjänimi ja salasana
exports.authenticate = (req, res) => {
    if (req.body.user === User.name) {
        if (req.body.password === User.password) {
            const payload = {
                'name': User.name
            };

            const token = jwt.sign(payload, secret, {
                expiresIn: 60 * 5 //Vanhenee 5 minuutissa
            });
            res.json({
                success: true,
                message: 'Alla oleva merkkijono on JWT-token, ota talteen!',
                token: token
            });
        }
    }
    res.json({
        success: false,
        message: 'Tunnus tai salasana on virheellinen'
    })
}