//Tässä harjoituksessa kokosin kaikki mongo CRUD funktiot samaan tiedostoon.

const Opiskelija = require('../opiskelijaSchema.js');

//Hae kaikki opiskelijat
exports.findAll = (req, res) => {
    Opiskelija.find()
        .then(opiskelijat => {
            res.send(opiskelijat);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'Error occured while retrieving opiskelijas'
            });
        });
};

exports.findOne = (req, res) => {
    Opiskelija.findById(req.params.opId)
        .then(opiskelija => {
            if (!opiskelija) {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            res.send(opiskelija);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            return res.status(500).send({
                message: 'Error retrieving opiskelija with id ' + req.params.opId
            });
        });
};

exports.findAllWithUnder100Sp = (req, res) => {
    Opiskelija.find({ opintopisteet: { $lt: 100 } })
        .then(opiskelijat => {
            res.send(opiskelijat);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'Error occured while retrieving opiskelijas with under 100 study points'
            });
        });
};

exports.create = (req, res) => {
    console.log(req.body);
    console.log(req.body.nimi);

    //Valitoidaan saatu data
    if (!req.body.opiskelijanumero) {
        return res.status(400).send({
            message: 'Opiskelijanumero can not be empty'
        });
    }

    //Luodaan opiskelija
    let opiskelija = new Opiskelija({
        opiskelijanumero: req.body.opiskelijanumero,
        nimi: req.body.nimi || 'Nimetön opiskelija',
        email: req.body.email || 'Ei sähköpostia'
    });

    //Tallennetaan uusi opiskelija tietokantaan
    opiskelija.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while creating new opiskelija'
            });
        });
};

exports.updateOne = (req, res) => {

    /*
    Luodaan olio, johon annetaan bodysta saatuja arvoja.
    Näin vain annetut kentät päivittyvät.
    Tämän voisi varmaan toteuttaa kauniimmin jollain loopilla,
    mukana voisi olla myös uuden datan validointia.
    */
    let update = new Object();

    if (req.body.opiskelijanumero) {
        update.opiskelijanumero = req.body.opiskelijanumero
    }
    if (req.body.nimi) {
        update.nimi = req.body.nimi
    }
    if (req.body.email) {
        update.email = req.body.email
    }

    Opiskelija.findByIdAndUpdate(req.params.opId, update, { new: true })
        .then(opiskelija => {
            if (!opiskelija) {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            res.send(opiskelija);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            return res.status(500).send({
                message: 'Error retrieving opiskelija with id ' + req.params.opId
            });
        });

};

exports.addCourse = (req, res) => {

    //Varmistetaan että kurssilla on nimi
    if (!req.body.kurssinimi) {
        return res.status(400).send({
            message: 'Kurssinimi can not be empty'
        });
    }

    //Luodaan kurssi
    let course = {
        'kurssinimi': req.body.kurssinimi,
        'laajuus': req.body.laajuus || 0,
        'arvosana': req.body.arvosana || 0
    };

    //Lisätään kurssi opiskelijalle
    Opiskelija.findByIdAndUpdate(req.params.opId, { $push: { kurssit: course } })
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while adding new course to opiskelija'
            });
        });

}

exports.editCourse = (req, res) => {

    /*
    En löytänyt tähän tehtävään ratkaisua.
    Ajatustasolla pitäisi etsiä kaikki kurssit joilla on sama kurssinimi,
    kaikista dokumenteissa olevista kurssit taulukoista.
    Sitten näille kaikille pitäisi toteuttaa update joka muokkaa arvot samalla tavalla.
    */
    if (!req.params.kursNimi) {
        return res.status(400).send({
            message: 'Kurssinimi can not be empty'
        });
    }

    let update = new Object();

    if (req.body.kurssinimi) {
        update.kurssinimi = req.body.kurssinimi
    }
    if (req.body.laajuus) {
        update.laajuus = req.body.laajuus
    }
    if (req.body.arvosana) {
        update.arvosana = req.body.arvosana
    }

    Opiskelija.findOneAndUpdate({ id: req.params.opId, 'kurssit.kurssinimi': req.params.kursNimi }, {
        $set: { 'kurssit': update }
    })
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || 'Some error occured while editing existing course'
            });
        });

}

exports.deleteOne = (req, res) => {
    Opiskelija.findByIdAndRemove(req.params.opId)
        .then(opiskelija => {
            if (!opiskelija) {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            res.send(opiskelija);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: 'Opiskelija not found with id ' + req.params.opId
                });
            }
            return res.status(500).send({
                message: 'Error deleting opiskelija with id ' + req.params.opId
            });
        });
}